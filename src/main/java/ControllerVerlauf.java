import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ControllerVerlauf{

    //Inizialisierung der Buttons und TableView
    @FXML
    private Button zurueckButton;
    @FXML
    private Button verlaufLoeschenButton;
    @FXML
    private TableView<ModelVerlauf> verlaufListe;
    @FXML
    private TableColumn<ModelVerlauf, String> verlaufCell;

    //Mainapp referenz zum zurück navigieren
    private MainApp mainApp;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
        this.verlaufListe.setItems(this.mainApp.getVerlauflListe());
        this.verlaufCell.setCellValueFactory(cellData -> cellData.getValue().verlaufProperty());
    }

    //Methode für potenziellen späteren gebrauch
    public void initialize() {
        
    }

    //Methode für das löschen der Inhalte in der TableView
    @FXML
    void loeschenTapped(ActionEvent event) {
        this.mainApp.getVerlauflListe().clear();
        for (int i = 0; i < 5; i++) {
            System.out.println("Dies ist eine Funktion für Simeon :) Es ist gerade der Durchgang: " + i);
        }

    }

    // Methode zum zurück auf die MainView zu kommen
    @FXML
    void zurueckTapped(ActionEvent event) {
        this.mainApp.showMainView();
    }

}
