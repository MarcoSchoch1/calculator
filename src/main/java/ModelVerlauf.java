import javafx.beans.property.SimpleStringProperty;

//SimpleStringProperty entgegen nahme und ausgabe für die TableView
public class ModelVerlauf {
    private SimpleStringProperty verlauf = new SimpleStringProperty();

    public ModelVerlauf(String verlauf) {
        this.setVerlauf(verlauf);
    }

    public SimpleStringProperty verlaufProperty() {
        return this.verlauf;
    }

    public void setVerlauf(String verlauf) {
        this.verlauf.set(verlauf);
    }

    public String getVerlauf() {
        return verlauf.get();
    }


}