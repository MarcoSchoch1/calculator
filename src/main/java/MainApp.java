import java.io.IOException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApp extends Application {

    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    //Inizialisierung der ObservableList, ist notwenig um sie in die TableView zu lesen
    private ObservableList<ModelVerlauf> verlaufListe = FXCollections.observableArrayList();

    //MainApp Konstruktor
    public MainApp() {
        
    }

    //Methode um die Liste auszugeben
    public ObservableList<ModelVerlauf> getVerlauflListe() {
        return this.verlaufListe;
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Rechner");
        this.showMainView();
    }

    //Methode um die MainView zu generieren und anzeigen zu lassen, zudem wird noch der Controller angebunden
    public void showMainView() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("/fxml/MainView.fxml"));
        
            Scene scene = new Scene(loader.load());
            primaryStage.setScene(scene);
            primaryStage.show();
            MainController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Methode um die VerlaufView zu generieren und anzeigen zu lassen, zudem wird noch der Controller angebunden
    public void showVerlaufView() {
		try {

			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("/fxml/VerlaufView.fxml"));
	
			Scene scene = new Scene(loader.load());
			this.primaryStage.setScene(scene);
	
			ControllerVerlauf controller = loader.getController();
			controller.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
        }
    }    
}