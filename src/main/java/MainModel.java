public class MainModel {
	
	//Hier findet die Berechnung statt
    public double calculate(double number1, double number2, String operation) {

		//Swichcase für die engegennahme des Operators
		switch (operation) {
		case "+":
			return number1 + number2;
		case "-":
			return number1 - number2;
		case "*":
			return number1 * number2;
		case "/":
			//Handeling der Division durch 0
			if (number2 == 0) {
				System.out.println("Durch 0 kann man nicht teilen");
				return 0;
			}
			return number1 / number2;

		}

		return 0;

	}
}