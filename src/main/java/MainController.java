import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;


public class MainController {


    //Instanzierung aller Variablen in dieser Klasse
    @FXML
    private Text resultLabel;
    @FXML
    private Button verlaufButton;
    
    private boolean isTypingNumber = false;
    private double firstNumber = 0;
    private double secondNumber = 0;
    private String firstNumberString;
    private String secondNumberString;
    private String resultString;
    private String operation = "";

    private MainModel model = new MainModel();

    //MainApp referenz
    private MainApp mainApp;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }    

    //Methode zum erkennen der Zahl die gedrückt ist
    @FXML
    public void numberTapped(ActionEvent event) {
        
        //Button wird gecastet und den Text genommen und abgespeichert
        String value = ((Button)event.getSource()).getText();

        //Sperren der Operatoren
        if (isTypingNumber) {
            resultLabel.setText(resultLabel.getText() + value);
        } else {
            resultLabel.setText(value);
            isTypingNumber = true;
        }
    }

    //Methode zum entgegen nehmen des Operators und das abspeichern der ersten Nummer
    @FXML
    public void calculatorTapped(ActionEvent event) {
        isTypingNumber = false;
        firstNumber = Double.parseDouble(resultLabel.getText());
        firstNumberString = Double.toString(firstNumber);

        operation = ((Button)event.getSource()).getText();
    }

    //Methode zum abspeichern der zweiten Nummer anzeige des Resultats
    @FXML
    public void equalsTapped(ActionEvent event) {
        isTypingNumber = false;
        secondNumber = Double.parseDouble(resultLabel.getText());
        secondNumberString = Double.toString(secondNumber);

        //einlesung für das Berechnen
        double result = model.calculate(firstNumber, secondNumber, operation);
        
        //rückgabe für die Anzeige
        resultLabel.setText(String.valueOf(result));
        resultString = Double.toString(result);

        //String für die TableView um deine Rechnung anzuzeigen
        String rechnung = firstNumberString + " " + operation + " " + secondNumberString + " = " + resultString;

        //adden in die Liste
        ModelVerlauf modelVerlauf = new ModelVerlauf(rechnung);
        this.mainApp.getVerlauflListe().add(modelVerlauf);
    }

    //Methode zum löschen der Anzeige
    @FXML
    public void restartTapped(ActionEvent event) {
        resultLabel.setText("");
        firstNumber = 0;
        secondNumber = 0;
    }

    //Methode zum springen auf die VerlaufsView
    @FXML
    void verlaufTapped(ActionEvent event) {
        System.out.println("Verlauf wurde gedrückt!");
        this.mainApp.showVerlaufView();
    }

    //Getter und Setter

    public double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Text getResultLabel() {
        return resultLabel;
    }

    public void setResultLabel(Text resultLabel) {
        this.resultLabel = resultLabel;
    }

    public String getFirstNumberString() {
        return firstNumberString;
    }

    public void setFirstNumberString(String firstNumberString) {
        this.firstNumberString = firstNumberString;
    }

    public String getSecondNumberString() {
        return secondNumberString;
    }

    public void setSecondNumberString(String secondNumberString) {
        this.secondNumberString = secondNumberString;
    }

    public String getResultString() {
        return resultString;
    }

    public void setResultString(String resultString) {
        this.resultString = resultString;
    }

}